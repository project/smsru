# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

SMS.ru — module integrates with sms.ru API to send SMS messages.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/smsru>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/smsru>


## REQUIREMENTS

This module requires no modules outside of Drupal core.

Suggest:

- drupal/sms can help to work with SMS via Drupal. If this module enabled,
integration will work automatically (because of Plugins).


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

There is no UI to configure module out of the box. This module is require you
to write a bit of code.


## MAINTAINERS

 * Nikita Malyshev (Niklan) - https://www.drupal.org/u/niklan
 * Andrei Ivnitskii - https://www.drupal.org/u/ivnish

